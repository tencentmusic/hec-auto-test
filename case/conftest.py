"""
 这是pytest中的预置函数定义的配置文件(注意:文件名称一定是conftest)
 scope参数定义的几种等级,默认等级是function:
    session:在本次session级别中只执行一次
    module:
    class:
    function:
    method:
"""
# 预置函数:用于数据准备
import time
import tkinter as tk

import pytest
from playwright.sync_api import Page

from pages.登录_page import Login
from pages.首页_page import HomePage
from utils.deleteFile import DeleteFile
from utils.weixin import WeiXinSend


def test_visit_admin_dashboard(page:Page):
    page.goto("http://cost.test.tmeoa.com/")

@pytest.fixture(scope="session")
def browser_context_args(browser_context_args):
    return {**browser_context_args,"ignoreHTTPSErrors":True}


@pytest.fixture(scope='session', autouse=True)
def delete():
    # 定期删除videos recording文件夹下3天前的文件
    DeleteFile('/Users/admin/test-results').delete(day=-3)
    DeleteFile('/private/tmp/').delete(day=0)


@pytest.fixture(scope="session")
def browser_context_args(browser_context_args):

    root = tk.Tk()
    分辨率_width = root.winfo_screenwidth()
    分辨率_height = root.winfo_screenheight()
    root.destroy()
    dict = {**browser_context_args,"viewport":{"width":分辨率_width,"height":分辨率_height,},
            'screen':{'width': 分辨率_width, 'height': 分辨率_height},
            'record_video_size':{'width': 分辨率_width, 'height': 分辨率_height},
            }
    return dict


@pytest.fixture(scope='class', autouse=True)
def login(page):
    """
    :param context: 浏览器实例
    1.新建网页page实例
    2.跳转到费控登录页,使用lingmuyazi登录
    yield page实例
    关闭网页
    """
    username = 'lingmuyazi'
    Login(page).login(value=username)
    HomePage(page).切换角色('员工-集团')
    yield
    # yield之后部分是后置,yield之前部分是前置



def pytest_terminal_summary(terminalreporter, exitstatus, config):
    '''收集测试结果'''
    # print(terminalreporter.stats)
    total = terminalreporter._numcollected
    passed = len([i for i in terminalreporter.stats.get('passed', []) if i.when != 'teardown'])
    failed = len([i for i in terminalreporter.stats.get('failed', []) if i.when != 'teardown'])
    error = len([i for i in terminalreporter.stats.get('error', []) if i.when != 'teardown'])
    skipped = len([i for i in terminalreporter.stats.get('skipped', []) if i.when != 'teardown'])
    successful = len(terminalreporter.stats.get('passed', [])) / total * 100
    # terminalreporter._sessionstarttime 会话开始时间
    duration = time.time() - terminalreporter._sessionstarttime
    print('total times: %.2f' % duration, 'seconds')

    # with open("../result.txt", "w") as fp:
    #     fp.write('total:%s' % total + "\n")
    #     fp.write('pass:%s' % passed + "\n")
    #     fp.write('failed:%s' % failed + "\n")
    #     fp.write('error:%s' % error + "\n")
    #     fp.write('skipped:%s' % skipped + "\n")
    #     fp.write('successful:%.2f%%' % successful + "\n")
    #     fp.write('duration:%s' % duration)
    #
    WeiXinSend(duration=duration, total=total, passed=passed, failed=failed, error=error, successful=successful)
