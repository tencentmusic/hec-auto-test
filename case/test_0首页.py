import allure
import pytest
from pytest_assume.plugin import assume
from pages.首页_page import HomePage
from playwright.sync_api._generated import Video

@allure.feature('首页功能')
class Test_0首页功能:

    @pytest.fixture(scope='function', autouse=True)
    def refresh(self, page):
        HomePage(page).goto_url('http://cost.test.tmeoa.com/homepage.screen')


    @pytest.mark.run(order=1)
    @allure.story("公告查看详情")
    def test_公告查看详情(self, context, page, browser):
        with context.expect_page() as new_page_info:
            HomePage(page).公告()
        new_page = new_page_info.value
        new_page.wait_for_load_state()
        with assume:
            assert new_page.title() == '文件预览'   # 判断点击第1条公告的【查看详情】，弹出的统一存储页面的title为“文件预览”，即测试通过
        new_page.wait_for_timeout(1500)
        new_page.screenshot()
        new_page.close(run_before_unload=True)


    @pytest.mark.run(order=2)
    @allure.story("意见反馈")
    def test_意见反馈(self, page):
        HomePage(page).意见反馈().screenshot()    # 判断意见反馈窗口，填写完后，能提交成功，即测试通过



    @pytest.mark.run(order=3)
    @allure.story("问题咨询")
    def test_问题咨询(self, page):
        HomePage(page).问题咨询()   # 判断问题咨询窗口，含“8000助手”字样，即测试通过

    @pytest.mark.run(order=4)
    @allure.story("系统操作指南")
    def test_系统操作指南(self, page, context):
        with context.expect_page() as new_page_info:
            HomePage(page).系统操作指南()
        new_page = new_page_info.value
        new_page.wait_for_load_state()
        with assume:
            assert new_page.title() == '文件预览'   # 判断点击系统操作指南的附件，弹出的统一存储页面的title为“文件预览”，即测试通过
        new_page.wait_for_timeout(1500)
        new_page.screenshot()
        new_page.close(run_before_unload=True)


    @pytest.mark.run(order=5)
    @allure.story("添加常用功能")
    def test_添加常用功能(self, page):
        HomePage(page).添加常用功能().screenshot() # 判断添加常用功能窗口，点击 ＋ 号不报错，即测试通过


    # @pytest.mark.run(order=6)
    # @allure.story("待办事项跳转")
    # def test_待办事项跳转(self, page):
    #     HomePage(page).待办事项跳转()
    #
    # @pytest.mark.run(order=7)
    # @allure.story("流程中的单据")
    # def test_流程中的单据(self, page):
    #     HomePage(page).流程中的单据()
