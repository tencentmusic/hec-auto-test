import allure
import pytest
from pages.国内差旅报销单_page import GuoNeiChailvBXDPage
from pages.附件和电子发票上传_page import UploadPage
from pages.业务审批_page import YeWuShenPiPage
from pages.登录_page import Login
from utils.faker_random import random_一句话
from pages.国内差旅计划申请单_page import GuoNeiChailvSQDPage

MS = random_一句话()

@allure.feature('国内差旅申请单')
class Test_20国内差旅计划申请单:

    @pytest.mark.run(order=201)
    @allure.story("国内差旅申请单_新建提交")
    @allure.severity('critical')
    def test_新建提交(self, page):
        global MS
        GuoNeiChailvSQDPage(page).差旅计划申请单icon().头部输入(MS=MS).行程行输入().住宿行输入().提交申请().screenshot()  # 国内差旅申请单，提交不报错，即测试通过
        assert True

    @pytest.mark.run(order=202)
    @allure.story("业务审批")
    @pytest.mark.parametrize('username', ['feifei',])
    def test_业务审批(self, page, username):
        Login(page).logout().login(value=username)
        result = YeWuShenPiPage(page).进入业务审批页面().单据类型_查询().同意()
        result.assert_text('text=共0条', '0', isFloat=True)


@allure.feature("国内差旅报销单")
class Test_21国内差旅报销单:

    @pytest.fixture(scope='function', autouse=True)
    def new国内差旅报销单(self, page):
        new = GuoNeiChailvBXDPage(page).国内差旅报销单icon()
        page.wait_for_timeout(1000)
        yield new

    @pytest.mark.run(order=211)
    @allure.story("新建和提交")
    @allure.title("新建和提交")
    @allure.severity('critical')
    def test_新建和提交(self, new国内差旅报销单, page):
        temp = new国内差旅报销单.头部输入(查询描述=MS).行程行输入().住宿行输入().其他行输入().保存草稿().收款方切换()
        UploadPage(page).附件和电子发票上传('/Users/admin/UItest/HecAutoTest/data/upload/阿里巴巴和华为笔试和面试题.pdf')  #   国内差旅报销单，上传附件测试
        result = temp.提交申请()
        result.assert_text('text=提交成功', '提交成功')     #   国内差旅报销单，提交后，弹窗含“提交成功”字样，即测试通过
