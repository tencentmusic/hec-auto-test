import allure
import pytest
from element.业务招待费报销单_element import ZhaodaifeiBxdElement
from pages.业务招待费报销单_page import ZhaodaifeiBxdPage


@allure.feature('业务招待费单case')
class Test_30业务招待费(ZhaodaifeiBxdElement):

    @pytest.fixture(scope='function', autouse=True)
    def new业务招待费(self, page):
        业务招待费新建页 = ZhaodaifeiBxdPage(page).业务招待费报销单icon()
        page.wait_for_timeout(1000)
        yield 业务招待费新建页
        page.click(self.ele_提交申请_确定)
        page.wait_for_timeout(200)

    @pytest.mark.run(order=301)
    @allure.story("我方招待人员为空,无法提交")
    @allure.title("我方招待人员为空")
    @allure.severity('critical')
    def test_我方招待人员不能为空提示(self, new业务招待费):
        result = new业务招待费.头部输入(申请人="屈天娥").行输入().提交申请()
        result.assert_text(self.ele_提交申请_确定_弹窗, "我方招待人员不能为空")  # 招待费-新建页，未填写我方招待人，提交后，弹窗含“我方招待人员不能为空”字样，即测试通过

    @pytest.mark.run(order=302)
    @allure.story("我方招待人员和申请人与付款主体不符,无法提交")
    @allure.title("我方招待人员和申请人与付款主体不符")
    def test_申请人我方招待人员与付款主体信息不符提示(self, new业务招待费):
        result = new业务招待费.头部输入(申请人='屈天娥', 跨BU="广州酷狗计算机科技有限公司").行输入().我方招待人('签约主体信息不符').提交申请()
        result.assert_text(self.ele_提交申请_确定_弹窗,
                           "申请人、我方招待人员与签约主体信息不符")  # 招待费-新建页，修改付款主体（使申请人和招待人的签约主体，与付款主体不在同一BU），提交后，弹窗含“我方招待人员不能为空”字样，即测试通过

    @pytest.mark.run(order=303)
    @allure.story("请遵守从高原则，由参与招待人员中管理级别最高者进行报销,无法提交")
    @allure.title("请遵守从高原则，由参与招待人员中管理级别最高者进行报销")
    def test_从高原则提示(self, new业务招待费):
        result = new业务招待费.头部输入(申请人='屈天娥', ).行输入().我方招待人('从高原则', ZDR='胡敏').提交申请()
        result.assert_text(self.ele_提交申请_确定_弹窗,
                           "请遵守从高原则，由参与招待人员中管理级别最高者进行报销")  # 招待费-新建页，我方招待人职位比申请人高，提交后，弹窗含“请遵守从高原则”字样，即测试通过

    @pytest.mark.run(order=304)
    @allure.story("业务招待人员符合规则,提交成功")
    @allure.title("业务招待人员符合规则")
    @allure.severity('critical')
    def test_提交成功(self, new业务招待费):
        result = new业务招待费.头部输入(申请人="屈天娥").行输入().我方招待人().保存草稿().提交申请()
        result.assert_text(self.ele_提交申请_确定_弹窗, "提交成功")  # 招待费-新建页，选择合规招待人，提交后，弹窗含“提交成功”字样，即测试通过

    @pytest.mark.run(order=305)
    @allure.story("对方单位不合规命中强校验关键字,无法提交")
    @allure.severity('critical')
    def test_对方单位强校验提示(self, new业务招待费):
        result = new业务招待费.头部输入(申请人="屈天娥").行输入(对方单位='版权局').我方招待人().提交申请()
        result.assert_text(self.ele_提交申请_确定_弹窗, "该单已违反FCPA反贿赂合规要求")  # 行字段【对方单位】含强校验文本，提交后，弹窗含“该单已违反FCPA反贿赂合规要求”字样，即测试通过

    @pytest.mark.run(order=306)
    @allure.story("对方单位+礼品不合规命中弱校验关键字,可以提交,收到不合规的邮件预警")
    def test_对方单位和礼品名称弱校验(self, new业务招待费):
        result = new业务招待费.头部输入(申请人="屈天娥").行输入(对方单位='主管部门', 礼品='礼品卡').我方招待人().提交申请()
        result.assert_text(self.ele_提交申请_确定_弹窗, "提交成功")  # 行字段【对方单位、礼品】含弱校验文本，提交后，弹窗含“提交成功”字样，即测试通过
