import allure
import pytest
from pages.借款申请单_page import JieKuanDanPage
from pages.借款申请单_查询_page import JieKuanDanChaXunPage

借款单号 = ''

allure.feature("借款申请单case")
class Test_40借款申请单_新建并审批:

    @pytest.mark.run(order=401)
    @allure.story("新建提交")
    @allure.title("新建提交")
    @allure.severity('critical')
    def test_新建提交测试(self, page):
        JieKuanDanPage(page).备用金借款单icon().头部输入().行输入().提交申请()
        temp = JieKuanDanChaXunPage(page).借款单申请单查询icon().查询().获取借款单号()
        global 借款单号
        借款单号 = temp.ele_借款单号
        temp.assert_text(temp.ele_单据编号, 借款单号)  # 借款单新建提交后，借款单查询页能查到该单号，即测试通过

