import allure
import pytest
from pages.其它费用报销单_page import QiTaFYBXDPage
from pages.资产录入_page import ZiChanLuRuPage
from pytest_assume.plugin import assume


@allure.feature('其他费用报销单')
class Test_50其他费用报销单:

    反冲单号 = ''

    @pytest.mark.run(order=501)
    @allure.story("提交成功测试")
    @allure.severity('critical')
    def test_提交成功测试(self,page):
        result = QiTaFYBXDPage(page).其它费用报销单icon().头部输入(申请人='屈天娥',付款主体='TENCENT MUSIC ENTERTAINMENT GROUP').行输入().提交申请()
        result.assert_text('text=提交成功', '提交成功')

        result.确定()

    @pytest.mark.run(order=502)
    @allure.story("整单删除测试")
    def test_整单删除测试(self, page):
        QiTaFYBXDPage(page).其它费用报销单icon().头部输入().行输入().保存草稿().整单删除()

    @pytest.mark.run(order=503)
    @allure.story("付款明细_保存草稿_添加审批人_提交成功测试")
    def test_跨BU报销_付款明细等测试(self, page):
        result1 = QiTaFYBXDPage(page).其它费用报销单icon().头部输入(申请人='屈天娥',部门名称='平台事业部/PC技术部',付款主体='广州酷狗计算机科技有限公司')\
            .添加审批人(审批人='付云霞').行输入().保存草稿().付款明细()
        result1.assert_text(result1.ele_报销总额, '2.00', True)
        result2 = result1.提交申请().确定()
        result2.assert_text('text=提交成功', '提交成功')


    @pytest.mark.run(order=504)
    @allure.story("核销借款提交测试")
    def test_核销借款提交测试(self, page):
        result1 = QiTaFYBXDPage(page).其它费用报销单icon().头部输入() \
            .行输入(金额='0.05').保存草稿().核销付款('0.1').保存草稿()
        result1.assert_text(result1.ele_实付总额, '0.00', True)
        result2 = result1.提交申请().确定()
        result2.assert_text('text=提交成功', '提交成功')

    @pytest.mark.run(order=505)
    @allure.story("资产未录入提示测试")
    def test_资产未录入提示测试(self, page):
        result1 = QiTaFYBXDPage(page).其它费用报销单icon().头部输入(申请人='屈天娥') \
            .导入数据().提交申请()
        result1.assert_text('text=资产信息未填写或与填单金额不匹配', '资产信息未填写或与填单金额不匹配')

    @pytest.mark.run(order=506)
    @allure.story("资产录入提交测试")
    def test_资产录入提交测试(self, page):
        result1 = QiTaFYBXDPage(page).其它费用报销单icon().头部输入(申请人='屈天娥').导入数据()
        global 反冲单号
        反冲单号 = ZiChanLuRuPage(page).资产录入().ele_bxd单号
        result1.提交申请()
        result1.assert_text('text=提交成功', '提交成功')

    @pytest.mark.run(order=507)
    @allure.story("操作指南")
    def test_操作指南测试(self, page, context):
        with context.expect_page() as new_page_info:
            result = QiTaFYBXDPage(page).其它费用报销单icon().操作指南()
        new_page = new_page_info.value
        new_page.wait_for_load_state()
        with allure.step('文件预览'):
            allure.attach(new_page.screenshot(animations="disabled"), '🪞测试截图🪞', allure.attachment_type.PNG)
            with assume:
                assert new_page.title() == '文件预览'  # 判断点击系统操作指南的附件，弹出的统一存储页面的title为“文件预览”，即测试通过
        new_page.wait_for_timeout(500)
        new_page.close(run_before_unload=True)

    @pytest.mark.run(order=508)
    @allure.story("填单示例")
    def test_填单示例测试(self, page, context):
        with context.expect_page() as new_page_info:
            QiTaFYBXDPage(page).其它费用报销单icon().填单示例()
        new_page = new_page_info.value
        new_page.wait_for_load_state()
        with allure.step('文件预览'):
            allure.attach(new_page.screenshot(animations="disabled"), '🪞测试截图🪞', allure.attachment_type.PNG)
            with assume:
                assert new_page.title() == '文件预览'  # 判断点击系统操作指南的附件，弹出的统一存储页面的title为“文件预览”，即测试通过
        new_page.wait_for_timeout(500)
        new_page.close(run_before_unload=True)

