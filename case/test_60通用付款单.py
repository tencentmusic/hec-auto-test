import allure
import pytest
from pages.供应商付款单_page import GongYingShangFKDPage
from pages.资产录入_page import ZiChanLuRuPage


@allure.feature('通用付款单')
class Test_60通用付款单case:

    @pytest.mark.run(order=601)
    @allure.story("提交成功测试")
    @allure.severity('critical')
    def test_提交成功测试(self,page):
        result = GongYingShangFKDPage(page).通用付款单icon().头部输入().行输入().提交申请()
        result.assert_text('text=提交成功', '提交成功')
        result.确定()
    #
    # @pytest.mark.run(order=602)
    # @allure.story("整单删除测试")
    # def test_整单删除测试(self, page):
    #     GongYingShangFKDPage(page).通用付款单icon().头部输入().行输入().保存草稿().整单删除()
    #
    # @pytest.mark.run(order=603)
    # @allure.story("核销借款提交测试")
    # def test_核销借款提交测试(self, page):
    #     result1 = GongYingShangFKDPage(page).通用付款单icon().头部输入() \
    #         .行输入(金额='1',税额='0.1').保存草稿().核销付款('0.1').保存草稿()
    #     result1.assert_text(result1.ele_实付总额, '0.9', True)
    #     result2 = result1.提交申请().确定()
    #     result2.assert_text('text=提交成功', '提交成功')
    #
    # @pytest.mark.run(order=604)
    # @allure.story("资产录入提交测试")
    # def test_资产录入提交测试(self, page):
    #     result1 = GongYingShangFKDPage(page).通用付款单icon().头部输入().导入数据()
    #     ZiChanLuRuPage(page).资产录入()
    #     result1.提交申请()
    #     result1.assert_text('text=提交成功', '提交成功')
