import allure
import pytest

from pages.TOC付款单_page import TocFKDPage


@allure.feature('TOC付款单')
class Test_61Toc付款单case:

    @pytest.mark.run(order=611)
    @allure.story("税金测算提交测试")
    @allure.severity('critical')
    def test_税金测算提交测试(self, page):
        result = TocFKDPage(page).TOC付款单icon().头部输入().行输入().税金测算().提交申请()
        result.assert_text('text=提交成功', '提交成功')
        result.确定()

    @pytest.mark.run(order=612)
    @allure.story("导入提交测试")
    def test_导入测试提交测试(self, page):
        result1 = TocFKDPage(page).TOC付款单icon().头部输入().导入数据().税金测算()
        result1.提交申请()
        result1.assert_text('text=提交成功', '提交成功')


