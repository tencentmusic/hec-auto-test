import allure
import pytest

from pages.登录_page import Login
from pages.部门经费调拨_page import BuMenJFDBPage
from pages.部门经费账户余额查询_page import BuMenJFZHYECXPage
from pages.首页_page import HomePage
from utils.tools import remove_千分位

AMOUNT = '10.00'
ZhuanChuYuE = ''
ZhuanRuYuE = ''
ZhuanChuZH = '虚拟账户-TME财经部/企业IT中心123-音乐北京'
ZhuanRuZH = '虚拟账户-TME财经部/企业IT中心123-香港HK'
@allure.feature("部门经费调拨case")
class Test_70部门经费调拨:

    @pytest.fixture(scope='class', autouse=True)
    def new部门经费调拨单(self, page):
        global ZhuanChuYuE, ZhuanRuYuE,ZhuanChuZH,ZhuanRuZH
        HomePage(page).切换角色('系统管理员-集团')
        temp = BuMenJFZHYECXPage(page).部门经费账户余额查询icon()
        ZhuanChuYuE = temp.账户余额查询(经费账户=ZhuanChuZH).ele_账户余额   # 新建调拨单前，先查询拿到，转出账户的余额，并赋值给全局变量ZhuanChuYuE
        temp.wait_for_timeout(2000)
        ZhuanRuYuE = temp.账户余额查询(经费账户=ZhuanRuZH).ele_账户余额    # 新建调拨单前，先查询拿到，转入账户的余额， 并赋值给全局变量ZhuanRuYuE
        page.wait_for_timeout(1000)
        new = BuMenJFDBPage(page).部门经费调拨icon()
        page.wait_for_timeout(1000)
        yield new

    @pytest.mark.run(order=701)
    @allure.story("部门经费调拨单-新建")
    @allure.severity('critical')
    def test_新建提交(self, new部门经费调拨单, page):
        global AMOUNT,ZhuanChuZH
        result = new部门经费调拨单.头部输入().行输入(From=ZhuanChuZH, amount=AMOUNT, to='TME财经部/企业IT中心123', zzh='香港HK').保存草稿().提交申请()
        result.assert_text('text=提交成功', '提交成功')     # 判断提交后的弹窗，含“提示成功”字样

    @pytest.mark.run(order=702)
    @allure.story("部门经费调拨单-查询")
    def test_查询(self, new部门经费调拨单, page):
        result = new部门经费调拨单.查询获取单号()
        result.assert_text(result.ele_第1条单据号, result.ele_经费调拨单号)    # 判断查询结果列表，存在新建的调拨单号

    @pytest.mark.run(order=703)
    @allure.story("部门经费调拨单-转出账户余额更新值正确")
    def test_转出账户余额更新值正确(self, new部门经费调拨单, page):
        global AMOUNT, ZhuanChuYuE,ZhuanChuZH
        result_zc = BuMenJFZHYECXPage(page).部门经费账户余额查询icon().账户余额查询(经费账户=ZhuanChuZH).ele_账户余额元素 # result_zc为转出账户的实际余额
        zhuanchu = remove_千分位(ZhuanChuYuE)
        amount = float(AMOUNT)
        zc = zhuanchu - amount  # zc为转出账户的预期余额
        new部门经费调拨单.assert_text(result_zc, zc, isFloat=True)  # 判断转出-账户实际余额 == 预期余额

    @pytest.mark.run(order=704)
    @allure.story("部门经费调拨单-转入账户余额更新值正确")
    def test_转入账户余额更新值正确(self, new部门经费调拨单, page):
        global AMOUNT, ZhuanRuYuE,ZhuanRuZH
        result_zr = BuMenJFZHYECXPage(page).部门经费账户余额查询icon().账户余额查询(经费账户=ZhuanRuZH).ele_账户余额元素  # result_zr为转入账户的实际余额
        zhuanru = remove_千分位(ZhuanRuYuE)
        amount = float(AMOUNT)
        zr = zhuanru + amount  # zr为转入账户的预期余额
        new部门经费调拨单.assert_text(result_zr, zr, isFloat=True)  # 判断转入-账户实际余额 == 预期余额



