import allure
import pytest

from pages.登录_page import Login
from pages.部门经费调整_page import BuMenJFTZPage
from pages.部门经费账户余额查询_page import BuMenJFZHYECXPage
from pages.业务审批_page import YeWuShenPiPage
from pages.首页_page import HomePage
from utils.tools import remove_千分位

AMOUNT = '100.00'
QiChuYuE = ''
@allure.feature("部门经费调整")
class Test_80部门经费调整:

    @pytest.fixture(scope='class', autouse=True)
    def new部门经费调整单(self, page):
        global QiChuYuE
        HomePage(page).切换角色('系统管理员-集团')
        QiChuYuE = BuMenJFZHYECXPage(page).部门经费账户余额查询icon().账户余额查询(经费账户='虚拟账户-TME财经部/企业IT中心123-音乐北京').ele_账户余额  # 新建调整单前，先查询拿到，账户的余额，并赋值给全局变量QiChuYuE
        page.wait_for_timeout(1000)
        new = BuMenJFTZPage(page).部门经费调整icon()
        page.wait_for_timeout(1000)
        yield new

    @pytest.mark.run(order=801)
    @allure.story("新建提交")
    @allure.severity('critical')
    def test_新建提交(self, new部门经费调整单, page):
        global AMOUNT
        result = new部门经费调整单.头部输入().行输入(调整类型='增',经费类型='团建经费',amount=AMOUNT,部门='TME财经部/企业IT中心123',zzh='音乐北京').保存草稿().提交申请()
        # 判断提交后的弹窗，含“提示成功”字样

    @pytest.mark.run(order=802)
    @allure.story("部门经费调整单查询页")
    @allure.severity('critical')
    def test_查询(self, new部门经费调整单):
        result = new部门经费调整单.查询获取单号()
        result.assert_text(result.ele_第1条单据号, result.ele_经费调整单号)    # 判断查询结果列表，存在新建的调整单号

    @pytest.mark.run(order=803)
    @allure.story("审批")
    def test_审批(self, new部门经费调整单, page):
        Login(page).logout().login(value='Fuyunxia', url=new部门经费调整单.baseurl)
        result = YeWuShenPiPage(page).进入业务审批页面().单号查询(单据编号=new部门经费调整单.ele_经费调整单号).同意()
        result.assert_element_not_exist(result.ele_单据号)     # 领导登录，对调整单进行审批操作，点击同意后，判断待审批列表不存在该单号就表示case测试通过

    @pytest.mark.run(order=804)
    @allure.story("账户余额更新值正确")
    def test_账户余额更新值正确(self, new部门经费调整单, page):
        global AMOUNT, QiChuYuE
        Login(page).logout().login(value='Lingmuyazi', url=new部门经费调整单.baseurl)
        result = BuMenJFZHYECXPage(page).部门经费账户余额查询icon().账户余额查询(经费账户='虚拟账户-TME财经部/企业IT中心123-音乐北京').ele_账户余额元素  # 拿到账户余额的实际余额
        a = remove_千分位(QiChuYuE)     # 将期初余额格式化，去掉千分位
        b = remove_千分位(AMOUNT)   # 将调整金额格式化
        c = a + b   # c = 期初余额 + 调整金额
        new部门经费调整单.assert_text(result, c, isFloat=True)  # 判断账户余额的实际余额 == 期初余额+调整金额



