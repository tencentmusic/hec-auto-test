import allure
import pytest
from pages.业务审批_page import YeWuShenPiPage
from pages.登录_page import Login
from pages.首页_page import HomePage


@allure.feature('其他费用报销单')
class Test_90业务审批:

    @pytest.mark.run(order=900)
    @allure.story("业务审批")
    @pytest.mark.parametrize('username', ['qutiane','ZHANGLULU','feifei', 'JEFFJZHOU', 'GRACEYTLUO','LINDA','CUSSIONPANG','lingmuyazi'])
    def test_业务审批(self, page, username):
        Login(page).logout().login(value=username)
        HomePage(page).切换角色(角色名称='员工-集团')
        result = YeWuShenPiPage(page).进入业务审批页面().单据类型_查询().同意()
        result.assert_text('text=共0条', '0', isFloat=True)