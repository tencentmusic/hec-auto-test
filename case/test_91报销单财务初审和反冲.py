
import allure
import pytest

from pages.报销单财务初审_page import BaoXDCaiWuChuShenPage
from pages.报销单财务反冲_page import BaoXDCaiWuFanChongPage
from pages.登录_page import Login
from pages.首页_page import HomePage

allure.feature("报销单财务初审和反冲case")
class Test_91报销单财务初审和反冲:

    @allure.story('初审通过')
    @pytest.mark.run(order=911)
    def test_初审通过(self, page):
        Login(page).logout().login(value='liyaru')
        HomePage(page).切换角色('财务初审(员工)-集团')
        BaoXDCaiWuChuShenPage(page).页面跳转().查询单据类型().创建凭证().审核()

    @allure.story('反冲')
    @pytest.mark.run(order=912)
    def test_反冲(self, page):
        # Login(page).logout().login(value='liyaru')
        BaoXDCaiWuFanChongPage(page).页面跳转().查询单据类型(单据类型='业务招待报销单').反冲()


