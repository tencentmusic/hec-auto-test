import pytest
import allure
import pytest_assume.plugin
from utils.common import HttpClient
from utils.logger import logger
from utils.yaml_util import read_yaml

log = logger(log_path='/Users/admin/UItest/HecAutoTest/data/jiekoutest.log', logging_name='HEC接口测试')
httpclient = HttpClient()


@allure.feature("税务影像回写费控接口")
class TestInterface:

    @allure.story("税务影像回写费控接口测试")
    @pytest.mark.parametrize('udata',read_yaml('/Users/admin/UItest/HecAutoTest/data/case.yaml'))
    def test123(self, udata):

        try:
            log.info(f"\n~~~~~~~~~~~~~~~~~~~接口用例标题:`{udata[0]['用例标题']}`~~~~~~~~~~~~~~~~~~~~~~~~~")
            # 实例化基准请求
            baseyaml = read_yaml('/Users/admin/UItest/HecAutoTest/data/base.yaml')
            # 判断case.yaml的替换参数,是属于第几层map,并进行参数替换
            if set(udata[2].keys()) < set(baseyaml['data'].keys()):
                log.info(f"操作步骤:\n 1.将'{list(udata[2].values())[0]}'传值给请求体[data]的'{list(udata[2].keys())[0]}'\n 2.发送接口请求")
                baseyaml['data'].update(udata[2])
            elif set(udata[2].keys()) < set(baseyaml['data']['list'][0].keys()):
                log.info(f"操作步骤:\n 1.将'{list(udata[2].values())[0]}'传值给请求体[data][list]的'{list(udata[2].keys())[0]}'\n 2.发送接口请求")
                for i in baseyaml['data']['list']:
                    i.update(udata[2])
            # 请求参数赋值,并发送request请求
            url = baseyaml['url']
            data = baseyaml['data']
            method = baseyaml['method']
            params_type = baseyaml['params_type']
            headers = baseyaml['headers']
            res = httpclient.send_request(method=method, url=url, params_type=params_type, data=data, headers=headers)
            # 获取用例的期望结果,实际结果
            excepted = udata[1]
            result = res.json()
            log.info(f"预期结果: message:{excepted}")
            log.info(f"实际结果: message:{result['message']}")
            # 返回报文记录到日志中
            log.info(f"打印返回报文:{res.text}")
            assert excepted['message'] in result['message'],log.info("message【测试失败】")
            assert excepted['code'] == result['code'],log.info("code【测试失败】")
        except Exception as e:
            log.info(f"\n—————————————————————————————————测试——失败—————————————————————————————————————————————————")
            assert False
        else:
            log.info(f"\n—————————————————————————————————测试通过—————————————————————————————————————————————————")

    if __name__ == '__main__':
        pytest.main(['test_接口测试.py','-sv'])

