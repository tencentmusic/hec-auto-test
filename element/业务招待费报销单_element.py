class ZhaodaifeiBxdElement:

    """frames"""
    ele_切换角色 = '//*[@id="pages-topbar-box1"]//div[2]/div[3]//div[1]/input'
    ele_frame1 = 'tabFrame1'
    ele_frame2 = 'tabFrame2'
    '''业务招待费报销单_头部输入'''
    ele_描述 = '#exp_rep_mt_standard_desc_ta'
    ele_业务招待报销单icon = "text=业务招待报销单"
    ele_申请人 = "//table[@id='exp_rep_mt_standard_normal_box']//tr[2]//td[2]//input"
    ele_项目 = "//table[@id='exp_rep_mt_standard_normal_box']//tr[4]//td[1]//input"
    ele_描述 = "#exp_rep_mt_standard_desc_ta"
    ele_部门名称 = "//table[@id='exp_rep_mt_standard_normal_box']//tr[3]//td[1]//input"
    ele_付款主体 = "//table[@id='exp_rep_mt_standard_normal_box']//tr[3]//td[2]//input"
    '''我方招待人员_填写'''
    ele_我方招待人员 = "#receptionists"
    ele_我方招待人员_左侧全选 = "//div[@id='employee_result_grid']/div//tbody/tr[2]//center/div"
    ele_我方招待人员_右侧全选 = "//div[@id='receptionists_grid']/div//tbody/tr[2]//center/div"
    ele_我方招待人员_选第1个 = "//*[@id='employee_result_grid']/div/div[2]//tbody/tr[2]//center/div"
    ele_我方招待人员_员工姓名 = "//*[@id='exp_report_receptionists_edit_window']/table//tr[2]//tr[2]/td//tr[1]//td[2]//input"
    ele_我方招待人员_查询 = "text=查询"
    ele_我方招待人员_右移按钮 = "button:has-text('→')"
    ele_我方招待人员_左移按钮 = "button:has-text('←')"
    ele_我方招待人员_保存按钮 = "button:has-text('保存')"
    '''业务招待费报销单_行输入'''
    ele_行新增 = "button:has-text('新增')"
    ele_日期从 = "[id*='grid_date_from_']"
    ele_日期从and日期至_输入 = "#datapicker_grid_dp input"
    ele_日期至 = "[id*='grid_date_to_']"
    ele_报销类型 = "[id*='grid_expense_type_name_']"
    ele_报销类型and费用项目_输入 = "#type_combox input"
    ele_费用项目 = "[id*='grid_exp_req_item_name_']"
    ele_金额 = "[id*='grid_unit_price_']"
    ele_金额_输入 = "#numberfield input"
    ele_人数 = "[id*='grid_guest_num_']"
    ele_人数_输入 = "#guest_nmf input"
    ele_对方单位 = "[id*='grid_guest_name_']"
    ele_对方单位_输入 = "#guest_txa"
    ele_礼品名称 = "[id*='grid_guest_gift_name_']"
    ele_礼品名称_输入 = "#guest_txt input"
    ele_说明事由 = "[id*='grid_description_']"
    ele_说明事由_输入 = "#description_grid_tf input"
    ele_行全选 = "//div[@id='grid']/div[1]/div[1]/table/tbody/tr[2]/td/center/div"
    ele_保存草稿 = "text=保存草稿"
    '''业务招待费报销单_提交申请'''
    ele_提交申请 = "text=提交申请"
    ele_提交申请_确定 = "text=确定"
    ele_assert_提交成功 = "text=提交成功"
    ele_提交申请_确定_弹窗 = "div[id*='aurora-msg-okext-gen'] .win-type"
    ele_提交申请_弹窗_确定 = "div[auskin='newbtnblue']"

