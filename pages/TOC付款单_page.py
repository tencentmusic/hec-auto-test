from element.TOC付款单_element import TocFKDElement
from utils.common import Common


class TocFKDPage(Common, TocFKDElement):

    def TOC付款单icon(self):
        self.goto_url(self.baseurl + self.ele_frame_url)
        self.click(self.ele_TOC付款单icon)
        self.wait_for_timeout(self.end_wait)
        return self

    def 头部输入(self, 接收币种='人民币', 部门名称=None, 付款主体=None, 合同=None, 有无合同=None):

        self.input(self.ele_接收币种, 接收币种)
        # self.input(self.ele_部门名称, 部门名称)
        # self.input(self.ele_付款主体, 付款主体)
        # self.input(self.ele_收款方, 收款方)
        self.input(self.ele_税金承担方式, '我方承担增值税及附加，对方承担个税')
        self.input(self.ele_个人所得类型, '劳务报酬所得')
        self.input(self.ele_所得子类, '设计')
        self.input(self.ele_说明, self.nowdate() + '\n' + " Toc付款单测试(#^.^#)  ", )
        if 有无合同 == None:
            pass
        else:
            self.click(self.ele_有无合同)
            self.input(self.ele_合同, 'CON04-TME00-20220427-0006')
        self.wait_for_timeout(self.end_wait)
        return self

    def 行输入(self, 付款类型='财务费用', 付款项目='财务费用', 填单金额='1000'):
        self.click(self.ele_新增)
        self.click(self.ele_发票日期)
        self.input(self.ele_发票日期_输入, self.localtime_加减n天(0), )
        self.click(self.ele_付款类型, )
        self.input(self.ele_付款类型_输入, 付款类型)
        self.click(self.ele_付款项目, )
        self.input(self.ele_付款项目_输入, 付款项目, )
        # self.click(self.ele_填单金额, )
        self.input(self.ele_填单金额_输入, 填单金额)
        self.click(self.ele_收款方, )
        self.input(self.ele_收款方_输入, '喵星人112233')
        self.click(self.ele_是否取得代开发票, )
        self.click(self.ele_已提供完税凭证税额, )
        self.input(self.ele_已提供完税凭证税额_输入, '100')
        self.dblclick(self.ele_行说明, )
        self.input(self.ele_行说明_输入, '张铭练自动化测试', )
        self.wait_for_timeout(self.end_wait)
        self.click(self.ele_行_全选)
        self.click(self.ele_新增)
        self.click(self.ele_收款方, )
        self.input(self.ele_收款方_输入, '员工供应商多账户还有不启用的')
        return self


    def 导入数据(self):
        self.click(self.ele_导入数据, )
        self.click(self.ele_导入数据_选择文件, )
        self.click_upload_files(self.ele_导入数据_选择文件_选择文件, r'./data/upload/TOC付款单导入.xls', )
        # self.assert_text(self.ele_导入数据_共n条, '2', True, )
        self.wait_for_timeout(500)
        self.click(self.ele_提交数据, )
        self.assert_text(self.ele_导入数据_数据导入成功, '数据导入成功', )
        self.click(self.ele_导入数据_数据导入成功_确定, )
        self.wait_for_timeout(self.end_wait)
        return self


    def 整单删除(self):
        self.click(self.ele_整单删除, )
        self.click(self.ele_整单删除_确定)
        self.assert_text('text=供应商付款申请单填写说明', '供应商付款申请单填写说明', )
        self.wait_for_timeout(self.end_wait)
        return self

    def 税金测算(self):
        self.click(self.ele_税金测算, )
        self.wait_for_timeout(1000)
        self.assert_text(self.ele_计税状态, '计算成功', )
        self.wait_for_timeout(self.end_wait)
        return self

    def 保存草稿(self):
        self.click(self.ele_保存草稿, )
        self.assert_text('text=共1条', '1', True, )
        self.wait_for_timeout(1200)
        return self

    def 提交申请(self):
        self.click(self.ele_提交申请, )
        self.click(self.ele_提交申请_确定, )
        self.wait_for_timeout(self.end_wait)
        return self

    def 确定(self):
        self.click(self.ele_提交申请_确定, )
        self.wait_for_timeout(self.end_wait)
        return self
