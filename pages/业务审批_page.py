import logging
from element.业务审批_element import YeWuShenPiElement
from utils.common import Common


class YeWuShenPiPage(Common, YeWuShenPiElement):

    def 进入业务审批页面(self):
        """

        :return:
        """

        self.goto_url(self.baseurl+self.ele_frame_url)
        self.wait_for_selector(self.ele_同意)
        self.wait_for_timeout(self.end_wait)

            
        return self

    def 单据类型_查询(self, 单据类型=''):
        """

        :param 单据类型:
        :return:
        """

        self.wait_for_timeout(2000)
        self.input(self.ele_单据类型, 单据类型)
        self.click(self.ele_更多)
        self.input(self.ele_申请日期从,self.localtime_加减n天(0))
        self.click(self.ele_查询)
        self.wait_for_timeout(1000)
        self.wait_for_selector(f'text={self.localtime_加减n天(0)}')
        self.input(self.ele_每页显示, '20')
        self.wait_for_timeout(5000)
        self.click(self.ele_全选)
        self.wait_for_timeout(self.end_wait)
        self.screenshot()
            
        return self

    def 单号查询(self, 单据编号):
        """

        :param 单据编号:
        :return:
        """

        self.wait_for_timeout(2000)
        self.click(self.ele_更多)
        self.input(self.ele_更多_单据号, 单据编号)
        self.click(self.ele_查询)
        self.wait_for_timeout(4000)
        self.wait_for_selector(f'text={self.localtime_加减n天(0)}')
        self.click(self.ele_全选)
        self.wait_for_timeout(1200)
        return self

    def 同意(self):
        """

        :return:
        """

        self.click(self.ele_同意)
        self.wait_for_timeout(6000)
        self.wait_for_selector('text=共0条')
        self.wait_for_timeout(self.end_wait)



        return self

    def 拒绝(self):
        """

        :return:
        """

        self.click(self.ele_拒绝)
        self.wait_for_timeout(self.end_wait)
        self.wait_for_selector('text=共0条')
        self.wait_for_timeout(self.end_wait)
            
        return self
