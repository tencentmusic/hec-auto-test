import logging

import pytest

from element.业务招待费报销单_element import ZhaodaifeiBxdElement
from utils.common import Common


class ZhaodaifeiBxdPage(Common, ZhaodaifeiBxdElement):

    def 业务招待费报销单icon(self):
        """
        1.跳转到switch_frame
        2.点击[业务招待费报销单]按钮,从而进入新建页
        :return:self链式调用
        """

        self.goto_url('http://cost.test.tmeoa.com//modules/expm/public/exp_report_type_choice.screen')
        self.click(self.ele_业务招待报销单icon)
        self.wait_for_timeout(self.end_wait)
        return self

    def 头部输入(self, 申请人=None, 跨BU=None):
        """
        1.新建页,头部输入
        2.跨BU不为空,则切换跨BU部门+业务实体场景
        3.申请人为空,则申请人和制单人一致,否则,授权填单场景
        :param 跨BU: 控制跨BU填单场景
        :param 申请人: 控制授权填单场景
        :return: self链式调用
        """

        if 跨BU is not None:
            self.input(self.ele_申请人, 申请人)
            self.input(self.ele_部门名称, "视频事业部/公共技术部/基础技术组")
            self.input(self.ele_付款主体, "广州酷狗计算机科技有限公司")
        elif 申请人 is not None:
            self.input(self.ele_申请人, 申请人)
        self.wait_for_timeout(500)
        self.input(self.ele_项目, '缺省')
        self.input(self.ele_描述, f'建单时间:{self.nowdate()},张铭练业务招待费自动化Test')
        self.wait_for_timeout(self.end_wait)
            
        return self

    def 行输入(self, 对方单位='国家电网', 礼品=None):
        """
        1.新建页,行部输入
        :param 对方单位: 控制对方单位强校验
        :param 礼品: 控制礼品强校验
        :return:
        """

        self.click(self.ele_行新增)
        self.click(self.ele_日期从)
        self.input(self.ele_日期从and日期至_输入, self.localtime_加减n天(-3))
        self.click(self.ele_日期至)
        self.input(self.ele_日期从and日期至_输入, self.localtime_加减n天(-1))
        self.click(self.ele_报销类型)
        self.input(self.ele_报销类型and费用项目_输入, '招待费')
        self.click(self.ele_费用项目)
        if 礼品 is not None:
            self.input(self.ele_报销类型and费用项目_输入, '礼品费用')
            self.wait_for_timeout(500)
            self.dblclick(self.ele_礼品名称)
            self.input(self.ele_礼品名称_输入, 礼品)
        else:
            self.input(self.ele_报销类型and费用项目_输入, '业务招待费')
        self.click(self.ele_金额)
        self.input(self.ele_金额_输入, '1001')
        self.click(self.ele_人数)
        self.input(self.ele_人数_输入, '2')
        self.click(self.ele_对方单位)
        self.input(self.ele_对方单位_输入, 对方单位)
        self.dblclick(self.ele_说明事由)
        self.input(self.ele_说明事由_输入, f'{self.nowdate()}')
        self.wait_for_timeout(self.end_wait)
            
        return self

    def 我方招待人(self, mode=None , ZDR='陈斌'):
        """
        1.进入我方招待人选择窗口,选择完后保存退出
        :param mode: 控制签约主体信息不符 从高原则的校验
        :return:
        """

        self.wait_for_timeout(1000)
        self.click(self.ele_我方招待人员)
        self.wait_for_timeout(1500)
        self.click(self.ele_我方招待人员_右侧全选)
        self.click(self.ele_我方招待人员_左移按钮)
        if mode == '签约主体信息不符':
            self.wait_for_timeout(500)
            self.click(self.ele_我方招待人员_选第1个)
        elif mode == '从高原则':
            self.wait_for_timeout(500)
            self.input(self.ele_我方招待人员_员工姓名, ZDR)
            self.click(self.ele_我方招待人员_左侧全选)
        else:
            self.click(self.ele_我方招待人员_左侧全选)
        self.click(self.ele_我方招待人员_右移按钮)
        self.wait_for_timeout(500)
        self.click(self.ele_我方招待人员_保存按钮)
        self.wait_for_timeout(self.end_wait)
            
        return self

    def 保存草稿(self):
        """
        新建页,点击保存按钮
        :return:
        """

        self.click(self.ele_保存草稿)
        self.wait_for_timeout(self.end_wait)
        return self

    def 提交申请(self):
        """
        新建页,点击提交申请按钮,弹窗再点击确定
        :return:
        """

        self.click(self.ele_提交申请)
        self.click(self.ele_提交申请_确定)
        self.wait_for_timeout(2000)
        self.wait_for_timeout(self.end_wait)
            
        return self




