from element.供应商付款单_element import GongYingShangFKDElement
from utils.common import Common


class GongYingShangFKDPage(Common, GongYingShangFKDElement):

    def 通用付款单icon(self):
        self.goto_url(self.baseurl+self.ele_frame_url)
        self.click(self.ele_备用金借款单icon)
        self.wait_for_timeout(self.end_wait)
        return self

    def 头部输入(self,接收币种='人民币',部门名称=None,付款主体=None,合同=None,收款方='喵星人112233',有无合同=None):

        self.input(self.ele_接收币种, 接收币种)
        # self.input(self.ele_部门名称, 部门名称)
        # self.input(self.ele_付款主体, 付款主体)
        self.input(self.ele_收款方, 收款方)
        self.input(self.ele_说明, self.nowdate()+'\n'+f" {接收币种}在{付款主体}的{部门名称}下报销发票,接收{接收币种}(#^.^#)  ",)
        if 有无合同 == None:
            self.click(self.ele_有无合同)
        else:
            self.input(self.ele_合同, 合同)
        self.wait_for_timeout(self.end_wait)
        return self

    def 行输入(self,付款类型='财务费用',付款项目='财务费用',金额='10',税额='1'):
        self.click(self.ele_新增)
        self.wait_for_timeout(600)
        self.click(self.ele_发票日期)
        self.input(self.ele_发票日期_输入, self.localtime_加减n天(0), )
        self.click(self.ele_付款类型, )
        self.input(self.ele_付款类型_输入, 付款类型)
        self.click(self.ele_付款项目, )
        self.input(self.ele_付款项目_输入, 付款项目, )
        self.click(self.ele_金额, )
        self.input(self.ele_金额_输入, 金额)
        self.click(self.ele_发票号码, )
        self.input(self.ele_发票号码_输入, 'Fp100101')
        self.wait_for_timeout(100)
        self.click(self.ele_税额, )
        self.input(self.ele_税额_输入, 税额 )
        self.dblclick(self.ele_行说明, )
        self.input(self.ele_行说明_输入, '张铭练自动化测试', )
        self.wait_for_timeout(self.end_wait)
        return self

    def 添加审批人(self, 审批人):
        self.click(self.ele_添加审批人, )
        self.wait_for_timeout(1000)
        self.click(self.ele_添加审批人_新增, )
        self.wait_for_timeout(1000)
        self.click(self.ele_添加审批人_新增_员工姓名)
        self.input(self.ele_添加审批人_新增_员工姓名_输入, 审批人, )
        self.click(self.ele_添加审批人_新增_确定, )
        self.wait_for_selector(self.ele_含专票, )
        self.wait_for_timeout(self.end_wait)
        return self

    def 导入数据(self):
        self.click('text=含专票')
        self.click(self.ele_导入数据, )
        self.click(self.ele_导入数据_选择文件, )
        self.click_upload_files(self.ele_导入数据_选择文件_选择文件, r'./data/upload/通用付款单导入.xls', )
        # self.assert_text(self.ele_导入数据_共n条, '2', True, )
        self.wait_for_timeout(500)
        self.click(self.ele_提交数据, )
        self.assert_text(self.ele_导入数据_数据导入成功, '数据导入成功', )
        self.click(self.ele_导入数据_数据导入成功_确定, )
        self.assert_text(self.ele_付款类型, '固定资产', )
        self.wait_for_timeout(self.end_wait)
        return self

    def 核销付款(self, 核销金额):
        self.click(self.ele_核销借款, )
        self.wait_for_timeout(self.end_wait)
        self.click(self.ele_本次核销金额, )
        self.input(self.ele_本次核销金额_输入, 核销金额, )
        self.click(self.ele_核销借款_确认, )
        self.wait_for_selector('text=从申请单创建')
        self.wait_for_timeout(1200)
        return self

    def 整单删除(self):
        self.click(self.ele_整单删除, )
        self.click(self.ele_整单删除_确定 )
        self.assert_text('text=供应商付款申请单填写说明', '供应商付款申请单填写说明', )
        self.wait_for_timeout(self.end_wait)
        return self

    def 保存草稿(self):
        self.click(self.ele_保存草稿, )
        self.assert_text('text=共1条', '1', True, )
        self.wait_for_timeout(1200)
        return self

    def 提交申请(self):
        self.click(self.ele_提交申请, )
        self.click(self.ele_提交申请_确定, )
        self.wait_for_timeout(self.end_wait)
        return self

    def 确定(self):
        self.click(self.ele_提交申请_确定, )
        self.wait_for_timeout(self.end_wait)
        return self


