import logging

import pytest

from element.借款申请单_element import JieKuanDanElement
from utils.common import Common
from utils.faker_random import random_个人档案, random_段落


class JieKuanDanPage(Common, JieKuanDanElement):

    def 备用金借款单icon(self):

        self.goto_url(self.baseurl+self.ele_frame_url)
        self.click(self.ele_备用金借款单icon)
        self.wait_for_timeout(self.end_wait)
            
        return self

    def 头部输入(self):

        self.input(self.ele_计划还款日, self.localtime_加减n天(7))
        self.input(self.ele_说明, f'{self.nowdate()}\n{random_个人档案()}')
        self.wait_for_timeout(self.end_wait)
        return self

    def 行输入(self):

        self.click(self.ele_新增)
        self.click(self.ele_借款类型)
        self.click(self.ele_借款类型_备用金)
        self.click(self.ele_借款类型_申请金额)
        self.input(self.ele_借款类型_申请金额_输入, '10')
        self.click(self.ele_借款类型_说明)
        self.input(self.ele_借款类型_说明_输入, random_段落())
        self.wait_for_timeout(self.end_wait)
            
        return self

    def 提交申请(self):

        self.click(self.ele_提交申请)
        self.click(self.ele_提交申请_确定)
        self.wait_for_timeout(3000)
        self.wait_for_timeout(self.end_wait)
            
        return self