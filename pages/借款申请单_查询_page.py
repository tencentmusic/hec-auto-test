import logging

import pytest

from element.借款申请单_查询_element import JieKuanDanChaXunElement
from utils.common import Common


class JieKuanDanChaXunPage(Common, JieKuanDanChaXunElement):
    def 借款单申请单查询icon(self):

        self.goto_url(self.baseurl + self.ele_frame_url)
        self.wait_for_selector('text=借款金额')
        self.wait_for_timeout(self.end_wait)
            
        return self

    def 查询(self):

        self.click(self.ele_查询)
        self.wait_for_timeout(self.end_wait)
            
        return self

    def 获取借款单号(self):

        self.ele_借款单号 = self.inner_text(self.ele_单据编号)
        self.wait_for_timeout(self.end_wait)
        return self
