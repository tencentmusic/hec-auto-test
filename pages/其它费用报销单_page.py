import random

from element.其它费用报销单_element import QiTaFYBXDElement
from utils.common import Common
from utils.faker_random import random_ssn, random_区


class QiTaFYBXDPage(Common, QiTaFYBXDElement):

    def 其它费用报销单icon(self):
        self.goto_url(self.baseurl+self.ele_frame_url)
        self.wait_for_selector('text=报销单填写说明', )
        self.click(self.ele_其它费用报销单icon, )
        self.wait_for_selector('text=如有对填单有疑问可咨询TME', )
        self.wait_for_timeout(self.end_wait)
        return self

    def 头部输入(self, 申请人='于佳鑫',接收币种='人民币',部门名称='TME企业发展部/三三四五工作室',付款主体='腾讯音乐'):
        self.input(self.ele_申请人, 申请人, )
        self.input(self.ele_接收币种, 接收币种, )
        self.input(self.ele_部门名称, 部门名称, )
        self.input(self.ele_付款主体, 付款主体, )
        self.input(self.ele_项目, '缺省', )
        self.wait_for_timeout(100)
        self.input(self.ele_描述, self.nowdate()+'\n'+f" {申请人}在{付款主体}的{部门名称}下报销发票,接收{接收币种}(#^.^#)  ", )
        self.click(self.ele_含专票, )
        self.click(self.ele_是否分摊, )
        self.wait_for_timeout(self.end_wait)
        return self

    def 行输入(self, 报销类型='人事类', 费用项目='人事类.人事福利.缺省', 发票币种='人民币',金额='1'):
        self.click(self.ele_新增, )
        self.click(self.ele_日期从, )
        self.input(self.ele_日期从_输入, self.localtime_加减n天(-7), )
        self.click(self.ele_日期到, )
        self.input(self.ele_日期到_输入, self.localtime_加减n天(0), )
        self.click(self.ele_报销类型, )
        self.input(self.ele_报销类型_输入, 报销类型, )
        self.click(self.ele_费用项目, )
        self.input(self.ele_费用项目_输入, 费用项目, )
        self.click(self.ele_部门, )
        self.input(self.ele_部门_输入, '1', )
        self.click(self.ele_发票币种, )
        self.input(self.ele_发票币种_输入, 发票币种, )
        self.wait_for_timeout(100)
        self.click(self.ele_金额, )
        self.input(self.ele_金额_输入, 金额, )
        self.click(self.ele_数量, )
        self.input(self.ele_数量_输入, '2', )
        self.click(self.ele_税额, )
        self.input(self.ele_税额_输入, '0.01', )
        self.dblclick(self.ele_说明, )
        self.input(self.ele_说明_输入, f'{报销类型}|{费用项目}|{发票币种}', )
        self.wait_for_timeout(self.end_wait)
        return self

    def 行_全选新增(self):
        self.click(self.ele_行_全选, )
        self.click(self.ele_新增, )
        self.wait_for_timeout(self.end_wait)
        return self

    def 导入数据(self):
        self.click(self.ele_含专票, )
        self.click(self.ele_导入数据, )
        self.click(self.ele_导入数据_选择文件, )
        self.click_upload_files(self.ele_导入数据_选择文件_选择文件, r'./data/upload/其他报销单导入.xls', )
        self.assert_text(self.ele_导入数据_共n条, '3', True, )
        self.click(self.ele_提交数据, )
        self.assert_text(self.ele_导入数据_数据导入成功, '数据导入成功', )
        self.click(self.ele_导入数据_数据导入成功_确定, )
        self.wait_for_selector(self.ele_含专票, )
        self.assert_text('text=固定资产', '固定资产', )
        self.wait_for_timeout(self.end_wait)
        return self

    def 核销付款(self, 核销金额):
        self.click(self.ele_核销借款, )
        self.wait_for_timeout(self.end_wait)
        self.click(self.ele_本次核销金额, )
        self.input(self.ele_本次核销金额_输入, 核销金额, )
        self.click(self.ele_核销借款_确认, )
        self.wait_for_timeout(self.end_wait)
        self.wait_for_selector(self.ele_含专票, )
        self.wait_for_timeout(self.end_wait)
        return self

    def 付款明细(self):
        self.click(self.ele_付款明细, )
        a = self.inner_text(self.ele_收款明细_付款金额, )
        self.click(self.ele_收款明细_付款金额, )
        self.input(self.ele_收款明细_付款金额_输入, str(float(a)-0.1), )
        self.click(self.ele_收款明细_新增, )
        self.input(self.ele_收款明细_付款金额_输入, '0.1', )
        self.input(self.ele_收款明细_收款方, '张璨')
        self.click(self.ele_收款明细_保存, )
        self.click(self.ele_收款明细_返回)
        self.wait_for_selector(self.ele_实际支付金额, )
        self.wait_for_timeout(2000)
        return self


    def 添加审批人(self, 审批人):
        self.click(self.ele_添加审批人, )
        self.wait_for_timeout(1000)
        self.click(self.ele_添加审批人_新增, )
        self.wait_for_timeout(1000)
        self.click(self.ele_添加审批人_新增_员工姓名)
        self.input(self.ele_添加审批人_新增_员工姓名_输入, 审批人, )
        self.click(self.ele_添加审批人_新增_确定, )
        self.wait_for_selector(self.ele_含专票, )
        self.wait_for_timeout(self.end_wait)
        return self

    def 整单删除(self):
        self.click(self.ele_整单删除, )
        self.click(self.ele_整单删除_确定 )
        self.assert_text('text=报销单填写说明', '报销单填写说明', )
        self.wait_for_timeout(self.end_wait)
        return self

    def 操作指南(self):
        self.click(self.ele_操作指南, )
        self.wait_for_timeout(self.end_wait)
        return self

    def 填单示例(self):
        self.click(self.ele_填单示例, )
        self.wait_for_timeout(self.end_wait)
        return self

    def 保存草稿(self):
        self.click(self.ele_保存草稿, )
        self.assert_text('text=共1条', '1', True, )
        self.wait_for_timeout(1200)
        return self

    def 提交申请(self):
        self.click(self.ele_提交申请, )
        self.click(self.ele_提交申请_确定, )
        self.wait_for_timeout(self.end_wait)
        return self

    def 确定(self):
        self.click(self.ele_提交申请_确定, )
        self.wait_for_timeout(self.end_wait)
        return self