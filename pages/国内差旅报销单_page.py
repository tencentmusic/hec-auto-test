import logging

import pytest

from element.国内差旅报销单_element import GuoNeiChailvBXDElement
from utils.common import Common


class GuoNeiChailvBXDPage(Common, GuoNeiChailvBXDElement):

    def 国内差旅报销单icon(self):

        self.goto_url(self.baseurl+self.ele_frame_url)
        self.click(self.ele_国内差旅费报销单icon)
        self.wait_for_timeout(self.end_wait)
            
        return self

    def 头部输入(self, 查询描述):

        self.input(self.ele_描述, 查询描述)
        self.click(self.ele_关联差旅计划lov)
        self.input(self.ele_关联差旅计划lov_描述, 查询描述)
        self.click(self.ele_关联差旅计划lov_查询)
        self.dblclick(f'text={查询描述}')
        self.wait_for_timeout(self.end_wait)
            
        return self

    def 行程行输入(self, 发票币种='人民币'):

        self.click(self.ele_行程行_新增)
        self.click(self.ele_行程行_日期从)
        self.input(self.ele_行程行_日期从_输入, self.localtime_加减n天(-3))
        self.click(self.ele_行程行_日期到)
        self.input(self.ele_行程行_日期到_输入, self.localtime_加减n天(0))
        self.click(self.ele_行程行_费用项目)
        self.input(self.ele_行程行_费用项目_输入, '国内差旅费-车船机票款')
        self.click(self.ele_行程行_交通工具)
        self.input(self.ele_行程行_交通工具_输入, '飞机')
        self.click(self.ele_行程行_出发地)
        self.input(self.ele_行程行_出发地_输入, '北京市')
        self.click(self.ele_行程行_到达地)
        self.input(self.ele_行程行_到达地_输入, '深圳市')
        self.click(self.ele_行程行_发票币种)
        self.input(self.ele_行程行_发票币种_输入, 发票币种)
        self.click(self.ele_行程行_发票金额)
        self.input(self.ele_行程行_发票金额_输入, '100')
        self.dblclick(self.ele_行程行_说明)
        self.input(self.ele_行程行_说明_输入, '行程行测试')

        self.wait_for_timeout(self.end_wait)
        return self

    def 住宿行输入(self, 发票币种='人民币'):

        self.click(self.ele_住宿行_新增)
        self.click(self.ele_住宿行_日期从)
        self.input(self.ele_住宿行_日期从_输入, self.localtime_加减n天(-3))
        self.click(self.ele_住宿行_日期到)
        self.input(self.ele_住宿行_日期到_输入, self.localtime_加减n天(0))
        self.click(self.ele_住宿行_地点)
        self.input(self.ele_住宿行_地点_输入, '深圳市')
        self.click(self.ele_住宿行_费用项目)
        self.click(self.ele_住宿行_费用项目_输入)
        self.click(self.ele_住宿行_发票币种)
        self.input(self.ele_住宿行_发票币种_输入, 发票币种)
        self.click(self.ele_住宿行_发票金额)
        self.input(self.ele_住宿行_发票金额_输入, '10')
        self.click(self.ele_住宿行_说明)
        self.input(self.ele_住宿行_说明_输入, '住宿行测试')
        self.wait_for_timeout(self.end_wait)
        return self

    def 其他行输入(self, 发票币种='人民币'):

        self.click(self.ele_其他行_新增)
        self.click(self.ele_其他行_日期从)
        self.input(self.ele_其他行_日期从_输入, self.localtime_加减n天(-3))
        self.click(self.ele_其他行_日期到)
        self.input(self.ele_其他行_日期到_输入, self.localtime_加减n天(0))
        self.click(self.ele_其他行_费用项目)
        self.input(self.ele_其他行_费用项目_输入, '国内差旅费-餐费')
        self.click(self.ele_其他行_发票币种)
        self.input(self.ele_其他行_发票币种_输入, 发票币种)
        self.click(self.ele_其他行_发票金额)
        self.input(self.ele_其他行_发票金额_输入, '1')
        self.dblclick(self.ele_其他行_说明)
        self.input(self.ele_其他行_说明_输入, '测试其他行')
        self.wait_for_timeout(self.end_wait)
            
        return self

    def 收款方切换(self, 收款方='屈天娥'):

        self.click(self.ele_收款方)
        self.click(self.ele_收款方lov)
        self.input(self.ele_收款方_收款方, 收款方)
        self.dblclick(self.ele_收款方_双击)

        self.wait_for_timeout(self.end_wait)
        return self

    def 保存草稿(self):

        self.click(self.ele_保存草稿)

        self.wait_for_timeout(self.end_wait)
        return self

    def 提交申请(self):

        self.click(self.ele_提交申请)
        self.click(self.ele_提交申请_确定)
        self.wait_for_timeout(2000)
        self.wait_for_timeout(self.end_wait)
            
        return self
