import logging

import pytest

from element.差旅计划申请单_element import GuoNeiChailvSQDElement
from utils.common import Common


class GuoNeiChailvSQDPage(Common, GuoNeiChailvSQDElement):

    def 差旅计划申请单icon(self):

        self.goto_url(self.baseurl+self.ele_frame_url)
        self.click(self.ele_icon)
        self.wait_for_timeout(self.end_wait)
            
        return self

    def 头部输入(self, MS):

        self.input(self.ele_描述, MS+f"\n{self.nowdate()}")
        self.wait_for_timeout(self.end_wait)
            
        return self

    def 行程行输入(self):

        self.click(self.ele_1新增)
        self.click(self.ele_1日期从)
        self.input(self.ele_1日期从_输入, self.localtime_加减n天(0))
        self.input(self.ele_1日期至_输入, self.localtime_加减n天(1))
        self.input(self.ele_1交通工具_输入, '飞机')
        self.input(self.ele_1发票金额_输入, '10')
        self.input(self.ele_1出发地_输入, '深圳市')
        self.input(self.ele_1到达地_输入, '北京市')
        self.input(self.ele_1说明_输入, f'{self.nowdate()}')
        self.wait_for_timeout(self.end_wait)
        return self

    def 住宿行输入(self):

        self.click(self.ele_2新增)
        self.click(self.ele_2日期从)
        self.input(self.ele_2日期从_输入, self.localtime_加减n天(0))
        self.input(self.ele_2日期至_输入, self.localtime_加减n天(2))
        self.input(self.ele_2住宿标准_输入, '100')
        self.input(self.ele_2地点_输入, '北京市')
        self.input(self.ele_2说明_输入, f'{self.nowdate()}')
        self.wait_for_timeout(self.end_wait)
        return self

    def 提交申请(self):

        self.click(self.ele_保存草稿)
        self.click(self.ele_提交申请)
        self.wait_for_selector(self.ele_提交申请_确定)
        self.click(self.ele_提交申请_确定)
        self.wait_for_timeout(1000)
        self.assert_element_not_exist('text=错误')
        return self