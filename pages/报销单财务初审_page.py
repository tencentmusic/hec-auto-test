from element.报销单财务初审_element import BaoXDCaiWuChuShenElement
from utils.common import Common
from utils.faker_random import random_文章


class BaoXDCaiWuChuShenPage(Common, BaoXDCaiWuChuShenElement):

    def 页面跳转(self):
        self.goto_url(self.baseurl+self.ele_frame_url)
        self.wait_for_timeout(1000)
        return self

    def 查询单号(self, 单号):
        self.input(self.ele_单据编号, 单号)
        self.click(self.ele_查询)
        self.wait_for_timeout(1000)
        self.assert_text(self.ele_列表_单据编号, 单号)
        return self

    def 查询单据类型(self, 单据类型=''):
        self.input(self.ele_单据类型, 单据类型)
        self.click(self.ele_更多)
        self.input(self.ele_报销日期从, self.localtime_加减n天(0))
        self.click(self.ele_查询)
        self.wait_for_timeout(1200)
        # self.assert_text(self.ele_列表_单据类型, 单据类型)
        return self

    def 创建凭证(self):
        self.click(self.ele_全选)
        self.click(self.ele_创建凭证)
        self.assert_text(self.ele_创建凭证成功, '凭证创建成功')
        self.click(self.ele_确定)
        self.wait_for_timeout(1000)
        return self

    def 审核(self):
        self.click(self.ele_全选)
        self.click(self.ele_审核)
        self.click(self.ele_确定)
        self.wait_for_timeout(6200)
        self.assert_text(self.ele_共0条, '0', isFloat=True)
        return self

    def 拒绝(self):
        self.click(self.ele_全选)
        self.click(self.ele_拒绝)
        self.input(self.ele_拒绝原因, random_文章())
        self.click(self.ele_确定)
        self.wait_for_timeout(1000)
        self.assert_text(self.ele_共0条, '0', isFloat=True)
        return self

