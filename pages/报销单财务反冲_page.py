from element.报销单财务反冲_element import BaoXDCaiWuFanChongElement
from utils.common import Common


class BaoXDCaiWuFanChongPage(Common, BaoXDCaiWuFanChongElement):

    def 页面跳转(self):
        self.goto_url(self.baseurl+self.ele_frame_url)
        self.wait_for_timeout(1000)
        return self

    def 查询单据类型(self, 单据类型):

        self.input(self.ele_单据类型, 单据类型)
        self.click(self.ele_更多)
        self.input(self.ele_报销日期从, self.localtime_加减n天(0))
        self.click(self.ele_查询)
        self.wait_for_timeout(2000)
        return self

    def 反冲(self):
        self.click('[id*="exp5150grid1__"]')
        self.input(self.ele_反冲日期, self.localtime_加减n天(0))
        self.screenshot()
        self.click(self.ele_反冲)
        self.click(self.ele_确定)
        self.wait_for_timeout(1000)
        self.screenshot()
        return self

