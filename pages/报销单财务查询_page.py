import logging

from element.报销单财务查询_element import BxdCaiWuChaXunElement
from utils.common import Common


class BxdCaiWuChaXunPage(Common, BxdCaiWuChaXunElement):

    单号 = ''
    def 跳转(self):
        self.goto_url(self.baseurl+self.ele_frame_url)
        return self

    def 查询(self, 单号, 单据类型):
        self.input(self.ele_单据编号, 单号)
        self.input(self.ele_单据类型, 单据类型)
        self.click(self.ele_查询)
        return self

    def 获取单号(self):
        global 单号
        单号 = self.inner_text(self.ele_单号)
        logging.error(单号)
        return 单号

