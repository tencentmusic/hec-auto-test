import logging

import allure
import pytest

from utils.common import Common
from utils.faker_random import random_ssn


class Login(Common):

    def login(self, value, url='http://passport.test.tmeoa.com/authorize?appkey=1a09ca3206b410f6e4b1a9e9f267c80d&redirect_uri=http%3A%2F%2Fcost.test.tmeoa.com%2Flogin.screen'):
        try:
            with allure.step(f'{value}进行登录{url}'):
                self.goto_url(url)
            self.input('#username', value)
            with self.page.expect_navigation(timeout=5000):
                self.page.fill('#password', 'password')
                # self.input('#password', 'password')
                self.click('#login')
            self.wait_for_selector('#page')
            self.wait_for_timeout(200)
            return self
        except Exception as e:
            logging.error(e)


    def logout(self):
        try:
            self.goto_url(self.baseurl)
            self.hover('#point-appear>img')
            self.wait_for_timeout(200)
            self.click('text=退出系统')
            self.click('text=确定')
            self.wait_for_selector('text=登录有疑问')
            self.wait_for_timeout(200)
            return self
        except Exception as e:
            logging.error(e)

