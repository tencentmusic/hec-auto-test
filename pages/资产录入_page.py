from element.资产录入_element import ZiChanLuRuElement
from utils.common import Common
from utils.faker_random import random_ssn


class ZiChanLuRuPage(Common, ZiChanLuRuElement):

    def 资产录入(self):
        random = random_ssn()
        self.click(self.ele_资产录入, )
        self.click('text=查询', )
        self.click(self.ele_资产录入_新增, )
        self.wait_for_timeout(500)
        self.click(self.ele_资产录入_行_单据号, )
        self.ele_bxd单号 = self.inner_text(self.ele_资产录入_弹窗_单据号, )
        self.dblclick(self.ele_资产录入_弹窗_单据号, )
        self.click(self.ele_资产录入_实物资产标签, )
        self.input(self.ele_资产录入_实物资产标签_输入, f'FeiK{random}', )
        self.click(self.ele_资产说明)
        self.input(self.ele_资产录入_资产说明_输入, f'戴尔笔记本电脑xpro{random}', )
        self.dblclick(self.ele_资产录入_资产数量, )
        self.input(self.ele_资产录入_资产数量_输入, '2', )
        self.dblclick(self.ele_资产录入_启用日期)
        self.input(self.ele_资产录入_启用日期_输入, self.localtime_加减n天(-2), )
        self.dblclick(self.ele_资产录入_资产原值)
        self.input(self.ele_资产录入_资产原值_输入, '10', )
        self.dblclick(self.ele_资产录入_放置地点)
        self.input(self.ele_资产录入_放置地点_输入, '上海', )
        self.dblclick(self.ele_资产录入_使用人)
        self.input(self.ele_资产录入_使用人_输入, '屈天娥', )
        self.dblclick(self.ele_资产录入_合同号)
        self.input(self.ele_资产录入_合同号_输入, f' CON-{self.nowdate()}-Xi ', )
        self.click(self.ele_资产录入_保存, )
        self.assert_text('text=共1条', '1', True, )
        self.click('[atype="window.close"]', )
        self.wait_for_selector('text=描述')
        self.wait_for_timeout(self.end_wait)
        return self