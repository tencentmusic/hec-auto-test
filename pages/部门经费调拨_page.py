import logging

import pytest

from element.部门经费调拨_element import BuMenJFDBElement
from utils.common import Common
from utils.faker_random import random_一句话


class BuMenJFDBPage(Common, BuMenJFDBElement):

    def 部门经费调拨icon(self):
        self.goto_url(self.baseurl+self.ele_frame_url)
        self.click(self.ele_新建调拨单)
        self.wait_for_timeout(self.end_wait)
            
        return self

    def 头部输入(self):

        self.input(self.ele_描述, f'{self.nowdate()}\n'+random_一句话())
        self.wait_for_timeout(self.end_wait)
        return self

    def 行输入(self, From, amount, to, zzh):

        self.click(self.ele_新增)
        self.click(self.ele_转出账户类型)
        self.click(self.ele_转出账户类型_选择虚拟账户)
        self.click(self.ele_转出经费账户)
        self.input(self.ele_转出经费账户_输入, From)
        self.click(self.ele_本次调拨金额)
        self.input(self.ele_本次调拨金额_输入, amount)
        self.click(self.ele_转入账户类型)
        self.click(self.ele_转入账户类型_选择)
        self.click(self.ele_转入部门)
        self.input(self.ele_转入部门_输入, to)
        self.click(self.ele_转入子账户)
        self.input(self.ele_转入子账户_输入, zzh)
        self.click(self.ele_调整事由)
        self.input(self.ele_调整事由_输入, random_一句话())
        self.wait_for_timeout(self.end_wait)
            
        return self

    def 保存草稿(self):

        self.click(self.ele_保存草稿)
        self.wait_for_timeout(self.end_wait)
            
        return self

    def 提交申请(self):

        self.click(self.ele_提交申请)
        self.click(self.ele_提交申请_确定)
        self.wait_for_timeout(self.end_wait)

        return self

    def 查询获取单号(self):

        self.input(self.ele_调拨日期从, self.localtime_加减n天(0))
        self.click(self.ele_查询)
        self.ele_经费调拨单号 = self.inner_text(self.ele_第1条单据号)
        self.wait_for_timeout(self.end_wait)
        return self