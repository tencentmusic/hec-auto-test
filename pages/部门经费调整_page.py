import logging

import pytest

from element.部门经费调整_element import BuMenJFTZElement
from utils.common import Common
from utils.faker_random import random_一句话


class BuMenJFTZPage(BuMenJFTZElement, Common):

    def 部门经费调整icon(self):

        self.goto_url(self.baseurl+self.ele_frame_url)
        self.click(self.ele_新建单据)

        self.wait_for_timeout(self.end_wait)
        return self

    def 头部输入(self):

        self.input(self.ele_描述, f'{self.nowdate()}\n'+random_一句话())

            
        return self

    def 行输入(self, 调整类型, 经费类型, amount, 部门, zzh):

        self.click(self.ele_新增)
        self.click(self.ele_调整类型)
        self.input(self.ele_调整类型_输入, 调整类型)
        self.click(self.ele_经费类型)
        self.input(self.ele_经费类型_输入, 经费类型)
        self.click(self.ele_金额)
        self.input(self.ele_金额_输入, amount)
        self.click(self.ele_账户类型)
        self.click(self.ele_账户类型_选择)
        self.click(self.ele_部门)
        self.input(self.ele_部门_输入, 部门)
        self.click(self.ele_子账户)
        self.input(self.ele_子账户_输入, zzh)
        self.click(self.ele_行备注)
        self.input(self.ele_行备注_输入, random_一句话())
        self.wait_for_timeout(self.end_wait)
            
        return self

    def 保存草稿(self):

        self.click(self.ele_保存草稿)

        self.wait_for_timeout(self.end_wait)
        return self

    def 提交申请(self):

        self.click(self.ele_提交申请)
        self.wait_for_timeout(100)
        self.click(self.ele_提交申请_确定)
        self.wait_for_timeout(1000)
        self.assert_text('text=提交成功', '提交成功')
        self.click(self.ele_提交申请_确定)
        self.wait_for_selector(self.ele_查询)
        self.wait_for_timeout(self.end_wait)
        return self

    def 导入数据(self, file):

        self.click(self.ele_导入数据)
        self.click(self.ele_选择文件)
        self.click_upload_files(self.ele_选择文件弹窗_选择文件, file)
        self.click(self.ele_提交数据)
        self.wait_for_timeout(self.end_wait)
            
        return self

    def 查询获取单号(self):

        self.input(self.ele_日期从, self.localtime_加减n天(0))
        self.click(self.ele_查询)
        self.ele_经费调整单号 = self.inner_text(self.ele_第1条单据号)
        self.wait_for_timeout(self.end_wait)
            
        return self
