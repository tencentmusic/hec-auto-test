import logging

import pytest

from element.部门经费账户余额查询_element import BuMenJFZHYECXElement
from utils.common import Common


class BuMenJFZHYECXPage(Common, BuMenJFZHYECXElement):

    def 部门经费账户余额查询icon(self):

        self.goto_url(self.baseurl+self.ele_frame_url)
        self.wait_for_selector('text=经费账户名')
        self.wait_for_timeout(self.end_wait)

        return self

    def 账户余额查询(self, 经费账户):

        self.input(self.ele_经费账户名, 经费账户)
        self.click(self.ele_查询)
        self.wait_for_timeout(3000)
        try:
            self.ele_账户余额 = self.inner_text(self.ele_账户余额元素)
        except:
            self.ele_账户余额 = 0
        self.wait_for_timeout(self.end_wait)
            
        return self