import logging

import pytest

from element.附件和电子发票上传_element import UploadElement
from utils.common import Common


class UploadPage(Common, UploadElement):

    def 附件和电子发票上传(self, files):

        self.click(self.ele_附件和电子发票上传)
        self.click_upload_files(self.ele_附件和电子发票上传_附件上传, files)
        self.wait_for_timeout(1000)
        self.click_upload_files(self.ele_附件和电子发票上传_电子发票上传, files)
        self.assert_text(self.ele_下载, '下载')
        self.click(self.ele_附件和电子发票上传_页面关闭)
        self.wait_for_timeout(500)

            
        return self