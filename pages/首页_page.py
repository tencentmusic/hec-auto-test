import logging

import pytest

from element.首页_element import HomeElement
from utils.common import Common
from utils.faker_random import random_文章


class HomePage(Common, HomeElement):

    def 退出登录(self):

        self.goto_url(self.baseurl)
        self.hover(self.ele_退出系统_用户名)
        self.wait_for_timeout(200)
        self.click(self.ele_退出系统)
        self.click(self.ele_退出系统_确定)
        self.wait_for_timeout(self.end_wait)
            
        return self

    def 切换角色(self, 角色名称):
        try:

            self.wait_for_timeout(200)
            self.press(self.ele_角色输入框, 'Enter')
            self.wait_for_timeout(100)
            self.page.fill(self.ele_角色输入框, 角色名称)
            self.wait_for_timeout(1200)
            self.press('html', 'Enter')
            if '员工' in 角色名称:
                self.wait_for_selector('text=费用报销单创建', '#tabFrame1')
            self.wait_for_timeout(self.end_wait)
            return self
        except Exception as e:
            logging.error(e)


    def 公告(self):
        self.wait_for_timeout(self.end_wait)
        return self.click(self.ele_第1条公告_查看详情, )


            

    def 意见反馈(self):

        self.click(self.ele_意见反馈, )
        self.input(self.ele_意见, f'张铭练自动化Test\n{self.nowdate()}\n'+random_文章(), )
        self.click(self.ele_意见反馈_确定, )

        return self

    def 问题咨询(self):

        self.click(self.ele_问题咨询, )
        self.assert_text(self.ele_assert8000, '8000', )
        self.click(self.ele_意见反馈_确定, )
        return self

    def 系统操作指南(self):
        return self.click(self.ele_TME荣誉激励费用管理要求和报销说明, )
            

    def 添加常用功能(self):

        self.click(self.ele_添加常用功能, )
        self.click(self.ele_添加常用功能_加号, )
        return self

    def 待办事项跳转(self):
        self.goto_url(self.baseurl)
        self.wait_for_timeout(2000)
        self.page.frame(url=self.ele_frame_url).wait_for_selector(self.ele_待办事项)
        self.page.frame(url=self.ele_frame_url).locator(self.ele_待办事项).click()
        self.wait_for_timeout(2000)
        pytest.assume(self.page.frame(url='/modules/wfl/WFL5111/workflowinstancelist_batch_approval.screen').locator('text=同意') == '同意')
            
        return self

    def 流程中的单据(self):
        self.wait_for_timeout(2000)
        self.page.frame(url=self.ele_frame_url).wait_for_selector(self.ele_流程中的单据)
        self.page.frame(url=self.ele_frame_url).locator(self.ele_流程中的单据).click()
        self.wait_for_timeout(2000)
        pytest.assume(self.page.frame(url='/modules/wfl/WFL5010/workflowpersonalactivelist.screen').locator('[id*="wfl_workflow_instance_tl_result_grid_back_"] a') == '收回')
        return self

    def 费用报销单创建icon(self):
        self.wait_for_selector('text=费控系统')
        self.click(self.ele_费用报销单创建icon, )
        return self

