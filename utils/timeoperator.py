#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author   : zhongxin
# @Time     : 2021/6/22 14:52
# @File     : timeoperator.py
# @Project  : WYTest
# @Desc     : 时间操作
import random
import time
import datetime
import calendar


class TimeOperator:
    @property
    def 当前时间戳(self):
        """
        返回当前时间戳
        :return:
        """
        return time.time()

    @property
    def 当前时间(self):
        """
        以 年-月-日 时:分:秒 格式返回当前时间
        :return:
        """
        return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

    @property
    def 当前年_月_日(self):
        """
        以 年-月-日 格式返回当前时间
        :return:
        """
        return time.strftime("%Y-%m-%d", time.localtime())

    @property
    def 当前年月日时分秒(self):
        """
        以 年月日时分秒 格式返回当前时间
        :return:
        """
        return time.strftime("%Y%m%d%H%M%S", time.localtime())

    @property
    def 当前年_月(self):
        """
        以 年-月 格式返回当前时间
        :return:
        """
        return time.strftime("%Y-%m", time.localtime())

    def 当前时间的n月后的年_月(self, add_num=0):
        """
        当前月份的n个月后的 年-月
        """
        y_m = self.now_month
        y, m = [int(i) for i in y_m.split("-")]
        m += add_num
        if m > 12:
            m -= 12
            y += 1
        return f'{y}-{m:02d}'

    def 当前时间的n日后的年_月_日_时分秒(self, day):
        """
        以 年-月-日 时:分:秒 格式返回当前时间+偏移时间
        :return:
        """
        return str(datetime.datetime.today() + datetime.timedelta(days=day)).split(".")[0]

    def get_age(self, birthday):
        """

        :param birthday: 年-月-日
        :return:
        """
        y, m, d = [int(i) for i in birthday.split('-')]
        birthday = datetime.date(y, m, d)
        today = datetime.date.today()
        return today.year - birthday.year - ((today.month, today.day) < (birthday.month, birthday.day))

    def get_year_month(self, year=0, month=0):
        now = datetime.datetime.now()
        if year == 0:
            year = now.year
        if month == 0:
            month = now.month
        return year, month

    def get_month_day(self, year=0, month=0):
        """
        获取指定月份第一天和最后一天
        """
        year, month = self.get_year_month(year, month)
        month_first_day, month_last_day = calendar.monthrange(year, month)
        return month_first_day, month_last_day

    def get_random_day(self, year=0, month=0):
        """
        获取指定年月的随机的一天
        """
        year, month = self.get_year_month(year, month)
        month_first_day, month_last_day = self.get_month_day(year, month)
        day = random.randint(month_first_day, month_last_day)
        return f'{year}-{month}-{day:02d}'

    def get_random_time(self):
        """
        获取随机 时:分:秒
        """
        h = random.randint(0, 23)
        m = random.randint(0, 59)
        s = random.randint(0, 59)
        return f'{h:02d}:{m:02d}:{s:02d}'

    def get_other_m_d(self, n=0, t=None):
        """
        获取n个月后的年月日范围
        """
        if t:
            y, m = t.split("-")
        else:
            y, m, _ = self.now2.split("-")
        y = int(y)
        m = int(m) + n
        d1, d2 = self.get_month_day(int(y), m)
        return f'{y:04d}-{m:02d}-{1:02d}', f'{y:04d}-{m:02d}-{d2:02d}'


timeoperator = TimeOperator()

# if __name__ == '__main__':
#     print(timeoperator.other_month())
#     print(timeoperator.other_month(1))
#     print(timeoperator.other_month(2))
