from re import sub


# 读取代码
def txtToDict(path):
    fr = open(path, 'r')
    dic = {}
    keys = []  # 用来存储读取的顺序
    for line in fr:
        v = line.strip().split(':')
        dic[v[0]] = v[1]
        keys.append(v[0])
    fr.close()
    return dic


def remove_千分位(value: any) -> float:
    val = sub(r'[^\d.]', '', str(value))
    return float(val)
