#
# class TxtWrite:
#
#     @staticmethod
#     def txt_write_n(txt, value):
#         with open(txt, "a+", encoding='utf-8') as f:
#             f.write(f'{value}\n')  # 这句话自带文件关闭功能，不需要再写f.close()
#
#     @staticmethod
#     def txt_write(txt, value):
#         with open(txt, "a+", encoding='utf-8') as f:
#             f.write(value)  # 这句话自带文件关闭功能，不需要再写f.close()
#
#     @staticmethod
#     def txt_clear(txt):
#         with open(txt, 'r+') as file:
#             file.truncate(0)
