import requests


def WeiXinSend(duration,total,passed,failed,error,successful):

    duration = '{:.2f}'.format(duration/60)
    headers = {"Content-Type": "text/plain"}
    send_url = "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=b4f8a3b5-af77-4088-8b1e-39c2d1139fa9"# 明英企业微信机器人的webhook
    尤跃的企业微信群_url = "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=4ecaa6de-e78d-4465-8c15-7d7db4baa40a" # 尤跃企业微信机器人
    if str(successful) == '100.0':
        emo = '🎉'
    else:
        emo = '😭'
    send_data = {
        "msgtype": "markdown",  # 消息类型，此时固定为markdown
        "markdown": {
            "content": f">**<font color=\"#0c212b\">"f"费控UI自动化测试结果{emo}</font> **\n"
                       f">**通过率：<font color=\"#003a6c\">{successful}%</font> **\n"  # 标题 （支持1至6级标题，注意#与文字中间要有空格）
                       f">用例总数：<font color=\"#1d953f\">{total}</font> \n" +  # 引用：> 需要引用的文字
                       f">成功用例数：<font color=\"#1d953f\">{passed}</font> \n" +  # 引用：> 需要引用的文字
                       f">失败用例数：<font color=\"#ed1941\">{failed}</font> \n" +  # 引用：> 需要引用的文字
                       f">报错用例数：<font color=\"#decb00\">{error}</font> \n" +  # 引用：> 需要引用的文字
                       f">执行耗时：<font color=\"#293047\">{duration}分</font> \n"  # 引用：> 需要引用的文字
                         # 加粗：**需要加粗的字**
        }
    }
    res = requests.post(url=send_url, headers=headers, json=send_data)
    youyue = requests.post(url=尤跃的企业微信群_url, headers=headers, json=send_data)
    # print(res.text)
