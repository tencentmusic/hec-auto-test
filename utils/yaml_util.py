# 专门把yaml文件的数据读取出来
from string import Template

import yaml


# 读取yaml文件
def read_yaml(yaml_file):
    # 打开yaml文件,读取方式为a追加,编码格式utf-8,然后重命名为f
    with open(yaml_file, mode='r', encoding='utf-8') as f:
        data = yaml.load(stream=f, Loader=yaml.FullLoader)
    return data

# 写入yaml文件
def write_yaml(data, yaml_file):
    with open(yaml_file, mode='a',encoding='utf-8') as f:
        # 写入的数据从data传入
        yaml.dump(data=data, stream=f, allow_unicode=True)

# 清空yaml文件
def clear_yaml(yaml_file):
    with open(yaml_file, mode='w',encoding='utf-8') as f:
        # 写入的数据从data传入
        f.truncate()

def yaml_relation(yaml_file, value: dict):
    """
    yaml文件内key值关联
    :param yaml_file: yaml文件路径
    :param value: key:value dict值
    :return: 解析并返回yaml文件
    """
    with open(yaml_file, encoding="utf-8") as f:
        re = Template(f.read()).substitute(value)
        return yaml.safe_load(re)
